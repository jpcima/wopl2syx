#include "OPLSynth.h"
#include "wopl_file.h"
#include <arpa/inet.h>
#include <getopt.h>
#include <vector>
#include <memory>
#include <stdio.h>
#include <unistd.h>
#include <sys/stat.h>

struct FILE_deleter
    { void operator()(FILE *h) { fclose(h); } };
typedef std::unique_ptr<FILE, FILE_deleter> u_FILE;

bool ConvertFromWOPL(
    unsigned insno,
    patchStruct &dst, patchMapStruct *melmap, percMapStruct *percmap,
    const WOPLInstrument &src)
{
    unsigned numpairs = 1;
    switch (src.inst_flags & WOPL_Ins_ALL_MASK) {
    case WOPL_Ins_2op: dst.bOp = PATCH_1_2OP; numpairs = 1; break;
    case WOPL_Ins_4op: dst.bOp = PATCH_1_4OP; numpairs = 2; break;
    case WOPL_Ins_Pseudo4op: dst.bOp = PATCH_2_2OP; numpairs = 2; break;
    default: return false;
    }

    const unsigned block[2] = {0b100, 0b100};
    dst.bAtB0[0] = block[0] << 2;
    dst.bAtC0[0] = (0b11 << 4) | src.fb_conn1_C0;
    if (dst.bOp != PATCH_1_2OP) {
        dst.bAtB0[1] = block[1] << 2;
        dst.bAtC0[1] = (0b11 << 4) | src.fb_conn2_C0;
    }

    for (unsigned opnum = 0; opnum < 4; ++opnum) {
        unsigned srcopnum = (opnum & 1) ? (opnum - 1) : (opnum + 1);
        dst.op[opnum].bAt20 = src.operators[srcopnum].avekf_20;
        dst.op[opnum].bAt40 = src.operators[srcopnum].ksl_l_40;
        dst.op[opnum].bAt60 = src.operators[srcopnum].atdec_60;
        dst.op[opnum].bAt80 = src.operators[srcopnum].susrel_80;
        dst.op[opnum].bAtE0 = src.operators[srcopnum].waveform_E0;
    }

    if (insno < 128 && melmap) {
        melmap->wBaseTranspose = src.note_offset1;

        if (dst.bOp == PATCH_2_2OP) {
            melmap->wSecondTranspose = src.note_offset2;
            melmap->wSecondFineTune = src.second_voice_detune;
        }
    }
    else if (insno < 256 && percmap) {
        percmap->bBaseNote = src.percussion_key_number;

        if (src.note_offset1)
            fprintf(stderr, "instrument %u: note offset(1) ignored (%d)\n",
                    insno, src.note_offset1);
        if (dst.bOp != PATCH_1_2OP) {
            if (src.note_offset1)
                fprintf(stderr, "instrument %u: note offset(2) ignored (%d)\n",
                        insno, src.note_offset1);
            if (src.second_voice_detune)
                fprintf(stderr, "instrument %u: voice2 finetune ignored (%d)\n",
                        insno, src.second_voice_detune);
        }
    }

    return true;
}

class Syx_Writer
{
public:
    Syx_Writer(std::vector<uint8_t> &buf)
        : buf(buf) {}

    size_t pos() const
        { return buf.size(); }

    void byte(unsigned b)
        { buf.push_back(b); }

    void byte2x4(unsigned b)
        { buf.push_back(b & 15);
          buf.push_back(b >> 4); }

    void short2x4(uint16_t s)
        { byte2x4(s & 255);
          byte2x4(s >> 8); }

    void string(const char *str, size_t len)
        { std::copy(str, str + len, std::back_inserter(buf)); }

    void checksum(size_t i1, size_t i2)
        { unsigned sum = 0;
          for (size_t i = i1; i < i2; ++i)
              sum = (sum + buf[i]) & 0x7f;
          byte((128 - sum) & 0x7f);
        }

private:
    std::vector<uint8_t> &buf;
};

void PatchToSyx(Syx_Writer &wr, unsigned insno, const patchStruct &pat)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3PATCH", 8);

    wr.byte2x4(insno);

    unsigned patchlen = sizeof(patchStruct);
    wr.byte2x4(patchlen);
    for (unsigned i = 0; i < patchlen; ++i)
        wr.byte2x4(((const uint8_t *)&pat)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}

void MelMapToSyx(Syx_Writer &wr, unsigned insno, patchMapStruct map)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3MLMAP", 8);

    wr.byte(insno);

    map.wBaseTranspose = htons(map.wBaseTranspose);
    map.wSecondTranspose = htons(map.wSecondTranspose);
    map.wPitchEGAmt = htons(map.wPitchEGAmt);
    map.wPitchEGTime = htons(map.wPitchEGTime);
    map.wBaseFineTune = htons(map.wBaseFineTune);
    map.wSecondFineTune = htons(map.wSecondFineTune);

    unsigned maplen = sizeof(patchMapStruct) - sizeof(patchMapStruct::bReservedPadding);
    wr.byte2x4(maplen);
    for (unsigned i = 0; i < maplen; ++i)
        wr.byte2x4(((const uint8_t *)&map)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}

void PercMapToSyx(Syx_Writer &wr, unsigned insno, percMapStruct map)
{
    wr.byte(0xf0);
    wr.byte(0x7d);
    wr.byte(0x10);  // device id
    wr.byte(0x00);  // placeholder for model id
    wr.byte(0x02);  // send

    size_t cs_start = wr.pos();
    wr.string("MaliceX ", 8);
    wr.string("OP3PCMAP", 8);

    wr.byte(insno - 128);

    wr.byte2x4(sizeof(map));
    for (unsigned i = 0; i < sizeof(map); ++i)
        wr.byte2x4(((const uint8_t *)&map)[i]);

    wr.checksum(cs_start, wr.pos());
    wr.byte(0xf7);
}

void BankToSyx(
    Syx_Writer &wr,
    const patchStruct pats[],
    const patchMapStruct melmaps[],
    const percMapStruct percmaps[],
    const std::vector<bool> &bitmap)
{
    unsigned inscount = bitmap.size();
    for (unsigned insno = 0; insno < inscount; ++insno) {
        if (!bitmap[insno])
            continue;
        PatchToSyx(wr, insno, pats[insno]);
        if (insno < 128) {
            patchMapStruct melmap = {};
            if (melmaps)
                melmap = melmaps[insno];
            else
                melmap.bPreset = insno;
            MelMapToSyx(wr, insno, melmap);
        }
        else {
            percMapStruct percmap = {};
            if (percmaps)
                percmap = percmaps[insno - 128];
            else
                percmap.bPreset = insno - 128;
            PercMapToSyx(wr, insno, percmap);
        }
    }
}

void BankToHeader(
    FILE *fh,
    const patchStruct pats[],
    const patchMapStruct melmaps[],
    const percMapStruct percmaps[],
    unsigned n,
    unsigned headerfmt)
{
    if (headerfmt < 1 || headerfmt > 2)
        throw std::runtime_error("Unknown header format requested");

    if (headerfmt == 2)
        fprintf(fh, "#include \"OPLPatch.h\"\n\n");

    fprintf(fh, "#ifdef __GNUC__\n"
            "#pragma GCC diagnostic push\n"
            "#pragma GCC diagnostic ignored \"-Wmissing-braces\"\n"
            "#endif\n\n");

    switch (headerfmt) {
    case 2: fprintf(fh, "const std::array<MelMap, 128> gbDefaultMelMap\n{{\n"); break;
    case 1: fprintf(fh, "patchMapStruct gbDefaultMelMap[128] =\n{\n"); break;
    }

    for (unsigned i = 0; i < 128; ++i) {
        const patchMapStruct &m = melmaps[i];
        fprintf(fh, "   {%3d,%3d,%3d,%3d,%3d,%3d,%3d,%3d},\n",
                m.bPreset,
                m.wBaseTranspose, m.wSecondTranspose,
                m.wPitchEGAmt,
                m.wPitchEGTime,
                m.wBaseFineTune, m.wSecondFineTune,
                m.bRetrigDly);
    }
    switch (headerfmt) {
        case 2: fprintf(fh, "}};\n\n"); break;
        case 1: fprintf(fh, "};\n\n"); break;
    }

    switch (headerfmt) {
        case 2: fprintf(fh, "const std::array<PercMap, 128> gbDefaultPercMap\n{{\n"); break;
        case 1: fprintf(fh, "percMapStruct gbDefaultPercMap[128] =\n{\n"); break;
    }

    for (unsigned i = 0; i < 128; ++i) {
        if (i > 0 && i % 12 == 0)
            fprintf(fh, "\n");
        const percMapStruct &m = percmaps[i];
        fprintf(fh, "   {%3d,%3d,%3d},",
                m.bPreset,
                m.bBaseNote,
                m.bPitchEGAmt);
    }
    fprintf(fh, "\n");
    switch (headerfmt) {
        case 2: fprintf(fh, "}};\n\n"); break;
        case 1: fprintf(fh, "};\n\n"); break;
    }

    switch (headerfmt) {
        case 2: fprintf(fh, "const std::array<Patch, 256> glpDefaultPatch\n{{\n"); break;
        case 1: fprintf(fh, "patchStruct glpDefaultPatch[128] =\n{\n"); break;
    }
    for (unsigned i = 0; i < 256; ++i) {
        if (i == 128)
            printf("\n   // Percussion voices (128-255)\n");
        const patchStruct &p = pats[i];
        printf("   { ");
        for (unsigned i = 0; i < sizeof(patchStruct); ++i)
            printf("0x%02x,", ((const uint8_t *)&p)[i]);
        printf(" },\n");
    }
    switch (headerfmt) {
        case 2: fprintf(fh, "}};\n\n"); break;
        case 1: fprintf(fh, "};\n\n"); break;
    }

    fprintf(fh, "#ifdef __GNUC__\n"
            "#pragma GCC diagnostic pop\n"
            "#endif\n");
}

enum class OutputMode {
    Sysex,
    Header,
};

struct WOPLFile_deleter
    { void operator()(WOPLFile *w) { WOPL_Free(w); } };
typedef std::unique_ptr<WOPLFile, WOPLFile_deleter> u_WOPLFile;

u_WOPLFile load_bank(const char *filename)
{
    u_FILE fh(fopen(filename, "rb"));
    if (!fh) {
        fprintf(stderr, "Cannot open bank file\n");
        return nullptr;
    }

    struct stat st;
    if (fstat(fileno(fh.get()), &st) != 0) {
        fprintf(stderr, "Cannot determine file size\n");
        return nullptr;
    }

    std::unique_ptr<uint8_t> data(new uint8_t[st.st_size]);
    if (fread(data.get(), 1, st.st_size, fh.get()) != st.st_size) {
        fprintf(stderr, "Cannot read bank file\n");
        return nullptr;
    }

    int err = 0;
    u_WOPLFile file(WOPL_LoadBankFromMem(data.get(), st.st_size, &err));
    if (!file) {
        fprintf(stderr, "Cannot load WOPL (error %d)\n", err);
        return nullptr;
    }
    return file;
}

int main(int argc, char *argv[])
{
    constexpr char usage[] = "%s <options> [-o output-file] wopl-file\n"
        "Options:\n"
        "    -H:   outputs a header file\n";

    if (argc < 2) {
        fprintf(stderr, usage, argv[0]);
        return 1;
    }

    const char *outputfile = nullptr;
    OutputMode outmode = OutputMode::Sysex;
    unsigned headerfmt = 0;
    for (int c; (c = getopt(argc, argv, "o:Hh")) != -1;) {
        switch (c) {
        case 'o':
            outputfile = optarg; break;
        case 'H':
            ++headerfmt;
            outmode = OutputMode::Header; break;
        case 'h':
        default:
            fprintf(stderr, usage, argv[0]);
            return 1;
        }
    }

    if (argc - optind != 1) {
        fprintf(stderr, usage, argv[0]);
        return 1;
    }
    const char *inputfile = argv[optind];

    patchStruct bank[256] = {};

    patchMapStruct melmaps[128] = {};
    percMapStruct percmaps[128] = {};
    for (unsigned i = 0; i < 128; ++i) {
        melmaps[i].bPreset = i;
        percmaps[i].bPreset = i;
    }

    //
    u_WOPLFile wopl = load_bank(inputfile);
    if (!wopl)
        return 1;

    WOPLBank nobank {};
    for (unsigned i = 0; i < 128; ++i)
        nobank.ins[i].inst_flags = WOPL_Ins_IsBlank;

    const WOPLBank &bmelo = (wopl->banks_count_melodic > 0) ? wopl->banks_melodic[0] : nobank;
    const WOPLBank &bperc = (wopl->banks_count_percussion > 0) ? wopl->banks_percussive[0] : nobank;

    size_t nmelodic = 128;
    size_t npercussive = 128;
    fprintf(stderr, "Melodic count: %zu\n", nmelodic);
    fprintf(stderr, "Percussive count: %zu\n", npercussive);

    std::vector<bool> patch_bitmap;
    patch_bitmap.resize(256);

    unsigned melo_count = 0;
    unsigned perc_count = 0;
    for (unsigned i = 0; i < 128; ++i) {
        patchStruct &patch = bank[i];
        if (ConvertFromWOPL(i, patch, &melmaps[i], nullptr, bmelo.ins[i])) {
            ++melo_count;
            patch_bitmap[i] = true;
        }
    }
    for (unsigned i = 0; i < 128; ++i) {
        patchStruct &patch = bank[i + 128];
        if (ConvertFromWOPL(i + 128, patch, nullptr, &percmaps[i], bperc.ins[i])) {
            ++perc_count;
            patch_bitmap[i + 128] = true;
        }
    }

    //
    FILE *fh = stdout;
    u_FILE fh_uniq;
    if (outputfile) {
        fh_uniq.reset(fopen(outputfile, "wb"));
        if (!fh)
            return 1;
        fh = fh_uniq.get();
    }

    switch (outmode) {
    case OutputMode::Sysex: {
        if (isatty(fileno(fh))) {
            fprintf(stderr, "not writing binary data to terminal\n");
            return 1;
        }

        std::vector<uint8_t> v;
        v.reserve(64 * 1024);

        Syx_Writer writer(v);
        BankToSyx(writer, bank, melmaps, percmaps, patch_bitmap);

        fwrite(v.data(), v.size(), 1, fh);
        break;
    }
    case OutputMode::Header:
        BankToHeader(fh, bank, melmaps, percmaps, 256, headerfmt);
        break;
    }

    if (fh) {
        if (fflush(fh) != 0) {
            if (outputfile)
                unlink(outputfile);
            return 1;
        }
    }

    return 0;
}
