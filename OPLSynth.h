#pragma once
#pragma pack(push)
#pragma pack(1)

typedef unsigned char   BYTE;
typedef unsigned short  WORD;

/* typedefs for MIDI patches */
#define PATCH_1_4OP  (0)        /* use 4-operator patch */
#define PATCH_2_2OP  (1)        /* use two 2-operator patches */
#define PATCH_1_2OP  (2)        /* use one 2-operator patch */

#define NUM2VOICES   18
#define NUM4VOICES   6 //9
#define NUMOPS       4
#define NUMMIDICHN   16

typedef struct _operStruct
{
   BYTE    bAt20;              /* flags which are send to 0x20 on fm */
   BYTE    bAt40;              /* flags seet to 0x40 */
   /* the note velocity & midi velocity affect total level */
   BYTE    bAt60;              /* flags sent to 0x60 */
   BYTE    bAt80;              /* flags sent to 0x80 */
   BYTE    bAtE0;              /* flags send to 0xe0 */
} operStruct;

typedef struct _patchStruct
{
   operStruct op[NUMOPS];      /* operators */
   BYTE    bAtA0[2];           /* send to 0xA0, A3 */
   BYTE    bAtB0[2];           /* send to 0xB0, B3 */
   /* use in a patch, the block should be 4 to indicate
   normal pitch, 3 => octave below, etc. */
   BYTE    bAtC0[2];           /* sent to 0xc0, C3 */
   BYTE    bOp;                /* see PATCH_??? */
   //BYTE    bDummy;             /* place holder */
   BYTE    bRhythmMap;         /* see RHY_CH_??? */
} patchStruct;

typedef struct _patchMapStruct
{
   BYTE bPreset;
   short wBaseTranspose, wSecondTranspose;
   short wPitchEGAmt;
   WORD wPitchEGTime;
   short wBaseFineTune, wSecondFineTune;
   BYTE bRetrigDly;
   BYTE bReservedPadding[8];
} patchMapStruct;

typedef struct _percMapStruct
{
   BYTE bPreset;
   BYTE bBaseNote;
   BYTE bPitchEGAmt;
} percMapStruct;

#pragma pack(pop)
