cmake_minimum_required(VERSION 3.6)

set(CMAKE_CXX_STANDARD 14)
#set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -fsanitize=address")

add_executable(wopl2syx
  wopl2syx.cc
  wopl_file.c)
